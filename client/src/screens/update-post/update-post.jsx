import * as React from 'react';
import { TextInput } from 'react-native';
import {
  ButtonVariant,
  HomeScreenName,
  IconName,
  NotificationMessage,
  TextVariant
} from 'common/enums/enums';
import { Button, Image, Text, View, Spinner } from 'components/components';
import { AppColor } from 'config/config';
import { pickImage } from 'helpers/helpers';
import {
  useDispatch,
  useNavigation,
  useState,
  useSelector,
  useEffect
} from 'hooks/hooks';
import {
  image as imageService,
  notification as notificationService
} from 'services/services';
import { threadActionCreator } from 'store/actions';
import styles from './styles';

const UpdatePost = () => {
  const dispatch = useDispatch();
  const navigator = useNavigation();
  const [body, setBody] = useState('');
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);
  // add update post option
  const { post } = useSelector(state => {
    const selected = state.posts.expandedPost;
    return { post: selected };
  });

  useEffect(() => {
    setImage({ link: post?.image?.link, id: post?.image?.id });
    setBody(post?.body);
  }, [post]);

  const handleUpdatePost = () => {
    if (!body) {
      return;
    }

    dispatch(
      threadActionCreator.updatePost({
        postId: post.id,
        imageId: image?.id,
        body
      })
    );
    setBody('');
    setImage(undefined);
    navigator.navigate(HomeScreenName.THREAD);
  };

  const handleUploadFile = async () => {
    setIsUploading(true);

    try {
      const data = await pickImage();
      if (!data) {
        return;
      }

      const { id, link } = await imageService.uploadImage(data);
      setImage({ id, link });
    } catch {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    } finally {
      setIsUploading(false);
    }
  };

  if (!post) {
    return <Spinner isOverflow />;
  }

  return (
    <View style={styles.screen}>
      <Text variant={TextVariant.HEADLINE}>Update Post</Text>
      <TextInput
        multiline
        value={body}
        placeholder="Type something here..."
        placeholderTextColor={AppColor.PLACEHOLDER}
        numberOfLines={10}
        style={styles.input}
        onChangeText={setBody}
      />
      <View style={styles.buttonWrapper}>
        <Button
          title="Update image"
          variant={ButtonVariant.TEXT}
          icon={IconName.PLUS_SQUARE}
          isLoading={isUploading}
          onPress={handleUploadFile}
        />
      </View>
      {image && (
        <Image
          style={styles.image}
          accessibilityIgnoresInvertColors
          source={{ uri: image?.link }}
        />
      )}
      <Button
        title="Update"
        isDisabled={!body || isUploading}
        onPress={handleUpdatePost}
      />
    </View>
  );
};

export default UpdatePost;
