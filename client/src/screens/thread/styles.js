import { DatePickerIOSBase, StyleSheet } from 'react-native';
import { AppColor } from 'config/config';

const styles = StyleSheet.create({
  header: {
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 15
  },
  logoText: {
    marginLeft: 5
  },
  filter: {
    padding: 10,
    backgroundColor: AppColor.BACKGROUND_DARK,
    // add show liked by me posts filter
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
});

export default styles;
