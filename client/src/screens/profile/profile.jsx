import * as React from 'react';
import {
  IconName,
  UserPayloadKey,
  ButtonVariant,
  NotificationMessage
} from 'common/enums/enums';
import { DEFAULT_USER_AVATAR } from 'common/constants/constants';
import {
  Button,
  Image,
  Input,
  Stack,
  View,
  TextInput
} from 'components/components';
import { useAppForm, useDispatch, useSelector, useState } from 'hooks/hooks';
import { profileActionCreator } from 'store/actions';
import {
  image as imageService,
  notification as notificationService
} from 'services/services';
import { pickImage } from 'helpers/helpers';
import styles from './styles';

const Profile = () => {
  const dispatch = useDispatch();
  const { user } = useSelector(state => ({
    user: state.profile.user
  }));
  const { control, errors } = useAppForm({
    defaultValues: {
      [UserPayloadKey.USERNAME]: user?.username,
      [UserPayloadKey.EMAIL]: user?.email,
      // add status option
      [UserPayloadKey.STATUS]: user?.status || null
    }
  });

  const handleUserLogout = () => dispatch(profileActionCreator.logout());

  // add update profile option
  const [edit, setEdit] = useState(false);
  const [isUploading, setIsUploading] = useState(false);
  const [editBody, setEditBody] = useState({
    username: user?.username,
    email: user?.email,
    status: user?.status,
    avatar: user?.image ? { link: user.image?.link, id: user.imageId } : null
  });

  const handleEdit = () => {
    setEdit(!edit);

    if (!edit) return;

    try {
      dispatch(
        profileActionCreator.update({
          username: editBody.username,
          email: editBody.email,
          status: editBody.status,
          imageId: editBody.avatar?.id || null
        })
      );
    } catch (err) {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    }
  };

  const handleCancel = () => {
    setEdit(false);
    setEditBody({
      ...editBody,
      username: user?.username,
      email: user?.email,
      avatar: { id: user?.imageId } || null
    });
  };

  const handleUpdateAvatar = async () => {
    setIsUploading(true);
    try {
      const data = await pickImage();
      if (!data) {
        return;
      }
      const { id, link } = await imageService.uploadImage(data);
      setEditBody({ ...editBody, avatar: { id, link } });
    } catch {
      notificationService.error(NotificationMessage.OPERATION_FAILED);
    } finally {
      setIsUploading(false);
    }
  };

  if (!user || isUploading) {
    return <></>;
  }

  return (
    <View style={styles.screen}>
      <View style={styles.content}>
        <Image
          style={styles.avatar}
          accessibilityIgnoresInvertColors
          source={{
            uri:
              user.image?.link || editBody.avatar?.link || DEFAULT_USER_AVATAR
          }}
        />
        {!edit && (
          <Stack space={15}>
            <Input
              name={UserPayloadKey.STATUS}
              control={control}
              errors={errors}
              iconName={IconName.STATUS}
              isDisabled
            />
            <Input
              name={UserPayloadKey.USERNAME}
              control={control}
              errors={errors}
              iconName={IconName.USER}
              isDisabled
            />
            <Input
              name={UserPayloadKey.EMAIL}
              control={control}
              errors={errors}
              iconName={IconName.ENVELOPE}
              isDisabled
            />
            <Button title="Logout" onPress={handleUserLogout} />
            <Button
              title="Edit"
              variant={ButtonVariant.TEXT}
              onPress={handleEdit}
            />
          </Stack>
        )}
        {edit && (
          <View>
            <Button
              title="Update avatar"
              variant={ButtonVariant.TEXT}
              icon={IconName.PLUS_SQUARE}
              isLoading={isUploading}
              onPress={handleUpdateAvatar}
            />
            <TextInput
              value={editBody.status}
              style={styles.input}
              onChangeText={v => setEditBody({ ...editBody, status: v })}
            />
            <TextInput
              value={editBody.username}
              style={styles.input}
              onChangeText={v => setEditBody({ ...editBody, username: v })}
            />
            <TextInput
              value={user?.email || editBody.email}
              style={styles.input}
              onChangeText={v => setEditBody({ ...editBody, username: v })}
            />
            <View style={styles.editButtons}>
              <Button
                title="Cancel"
                variant={ButtonVariant.TEXT}
                onPress={handleCancel}
              />
              <Button
                title="Submit"
                variant={ButtonVariant.TEXT}
                onPress={handleEdit}
              />
            </View>
          </View>
        )}
      </View>
    </View>
  );
};

export default Profile;
