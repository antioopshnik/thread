import { StyleSheet } from 'react-native';
import { AppColor } from 'config/config';

const styles = StyleSheet.create({
  screen: {
    alignItems: 'center',
    padding: 15,
    paddingTop: 20
  },
  content: {
    minWidth: 240
  },
  avatar: {
    marginBottom: 30,
    width: 250,
    height: 250,
    borderRadius: 125
  },
  editButtons: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    borderRadius: 5,
    borderColor: AppColor.BACKGROUND_DARK,
    backgroundColor: AppColor.BACKGROUND_DARK,
    color: AppColor.PLACEHOLDER,
    fontWeight: '400',
    fontSize: 16
  }
});

export default styles;
