import { HttpMethod, ContentType } from 'common/enums/enums';

class Post {
  constructor({ apiPath, http }) {
    this._apiPath = apiPath;
    this._http = http;
  }

  getAllPosts(filter) {
    return this._http.load(`${this._apiPath}/posts`, {
      method: HttpMethod.GET,
      query: filter
    });
  }

  getPost(id) {
    return this._http.load(`${this._apiPath}/posts/${id}`, {
      method: HttpMethod.GET
    });
  }

  // add dislike option
  getPostReaction(postId) {
    return this._http.load(`${this._apiPath}/posts/react/${postId}`, {
      method: HttpMethod.GET
    });
  }

  // add delete post option
  deletePost(id) {
    return this._http.load(`${this._apiPath}/posts/${id}`, {
      method: HttpMethod.DELETE
    });
  }

  // add update post option
  updatePost(id, payload) {
    return this._http.load(`${this._apiPath}/posts/${id}`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  addPost(payload) {
    return this._http.load(`${this._apiPath}/posts`, {
      method: HttpMethod.POST,
      contentType: ContentType.JSON,
      payload: JSON.stringify(payload)
    });
  }

  likePost(postId) {
    return this._http.load(`${this._apiPath}/posts/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: true
      })
    });
  }

  // add dislike option

  dislikePost(postId) {
    return this._http.load(`${this._apiPath}/posts/react`, {
      method: HttpMethod.PUT,
      contentType: ContentType.JSON,
      payload: JSON.stringify({
        postId,
        isLike: false
      })
    });
  }
}

export { Post };
