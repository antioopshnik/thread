const UserPayloadKey = {
  USERNAME: 'username',
  EMAIL: 'email',
  PASSWORD: 'password',
  // add status option
  STATUS: 'status'
};

export { UserPayloadKey };
