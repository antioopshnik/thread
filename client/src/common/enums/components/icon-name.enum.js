const IconName = {
  CAT: 'cat',
  COMMENT: 'comment',
  ENVELOPE: 'envelope',
  EXCLAMATION_TRIANGLE: 'exclamation-triangle',
  HOME: 'home',
  INFO: 'info',
  LOCK: 'lock',
  PAPER_PLANE: 'paper-plane',
  PLUS_SQUARE: 'plus-square',
  SHARE_ALT: 'share-alt',
  THUMBS_UP: 'thumbs-up',
  THUMBS_DOWN: 'thumbs-down',
  USER: 'user',
  // add delete post option
  DELETE_POST: 'trash-alt',
  // add update post option
  UPDATE_POST: 'marker',
  // add status option
  STATUS: 'quote-left'
};

export { IconName };
