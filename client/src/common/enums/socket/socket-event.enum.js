const SocketEvent = {
  CREATE_ROOM: 'createRoom',
  LEAVE_ROOM: 'leaveRoom',
  NEW_POST: 'newPost',
  LIKE: 'like',
  DISLIKE: 'dislike'
};

export { SocketEvent };
