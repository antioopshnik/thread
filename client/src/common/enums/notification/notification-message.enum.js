const NotificationMessage = {
  OPERATION_FAILED: 'Something went wrong',
  LIKE: 'Your post was liked!',
  DISLIKE: 'Your post was disliked!'
};

export { NotificationMessage };
