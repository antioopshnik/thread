const ActionType = {
  ADD_POST: 'thread/add-post',
  // add delete post option
  DELETE_POST: 'thread/delete-post',
  // add update post option
  UPDATE_POST: 'thread/update-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  REACT: 'thread/react',
  COMMENT: 'thread/comment'
};

export { ActionType };
