const getCommentsCountQuery = model =>
  model.relatedQuery('comments').count().as('commentCount');

const getReactionsQuery = model => isLike => {
  const col = isLike ? 'likeCount' : 'dislikeCount';

  return model.relatedQuery('postReactions').count().where({ isLike }).as(col);
};

const getWhereUserIdQuery = userId => builder => {
  if (userId) {
    builder.where({ userId });
  }
};

// add show liked by me posts filter
const getWithMyLike = ({ model, userId, liked }) => {
  if (liked) {
    return model
      .relatedQuery('postReactions')
      .where('isLike', true)
      .andWhere('userId', userId);
  }
  // dummy request that always returns smth if at least one post exists
  return model.query().select('posts.*');
};

export {
  getCommentsCountQuery,
  getReactionsQuery,
  getWhereUserIdQuery,
  getWithMyLike
};
