import { ExceptionMessage } from '../../common/enums/enums';
import { InvalidCredentialsError } from '../../exceptions/exceptions';

class User {
  constructor({ userRepository }) {
    this._userRepository = userRepository;
  }

  async getUserById(id) {
    const user = await this._userRepository.getUserById(id);

    return user;
  }

  async getUser(filter) {
    const user = await this._userRepository.getUser(filter);

    return user;
  }

  // add update user profile option
  async update(id, data) {
    const { username } = data;
    const user = await this._userRepository.getUser(id, { username });
    if (user) {
      throw new InvalidCredentialsError(
        ExceptionMessage.USERNAME_ALREADY_EXISTS
      );
    }
    const patched = await this._userRepository.updateById(id, {
      ...data
    });
    return patched;
  }
}

export { User };
